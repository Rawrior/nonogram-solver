import pytest
from solver import SolverClass

SOLVER = None


@pytest.fixture(scope="function", autouse=True)
def set_up_game():
    # global SOLVER
    SOLVER = SolverClass(True)


@pytest.mark.skip(reason="Function doesn't exist anymore")
def test_log(capsys):
    expected = "test String\n"
    SOLVER.log("test String")
    out, _ = capsys.readouterr()
    assert out == expected
